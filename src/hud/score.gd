extends Label

onready var game = get_node("/root/game")

func _ready():
	autoload.recent_score = 0

func _on_timer_timeout():
	for node in get_tree().get_nodes_in_group("tree"):
		if not node.burned:
			autoload.recent_score += 1
	
	text = "%d survivors" % autoload.recent_score