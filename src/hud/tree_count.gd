extends TextureProgress

func _ready():
	max_value = get_tree().get_nodes_in_group("tree").size()

func _process(delta):
	var alive = 0
	for node in get_tree().get_nodes_in_group("tree"):
		if not node.burned:
			alive += 1
	value = alive
	if value <= 38:
		get_node("/root/game").emit_signal("game_over")