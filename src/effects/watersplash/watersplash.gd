extends Particles2D

func _on_extinguish_timer_timeout():
	if not emitting:
		return

	for body in $area.get_overlapping_bodies():
		if body.is_in_group("burnable"):
			body.burning = false
