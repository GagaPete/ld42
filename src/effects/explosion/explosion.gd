extends Particles2D

func _ready():
	emitting = true

func _on_timer_timeout():
	for body in $area.get_overlapping_bodies():
		if body.is_in_group("burnable"):
			body.burning = true
	queue_free()