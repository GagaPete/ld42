extends Timer

const SATELLITE_SCENE = preload("res://objects/satellite/satellite_drop.tscn")

func _ready():
	var satellite = SATELLITE_SCENE.instance()
	$"..".add_child(satellite)
	satellite.position = $"../first_impact_point".position

func _on_satellite_spawner_timeout():
	randomize()
	var burnables = get_tree().get_nodes_in_group("burnable")
	var burnable = burnables[randi() % burnables.size()]
	var satellite = SATELLITE_SCENE.instance()
	get_parent().add_child(satellite)
	satellite.global_position = burnable.global_position + Vector2(0, 120 + (randi() % 40)).rotated(PI * ((randi() % 360) / 180))