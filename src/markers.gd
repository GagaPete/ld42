extends CanvasLayer

const MARKER_SCENE = preload("res://marker/marker.tscn")
const WIDTH = 1920
const HEIGHT = 1080
const SAFEZONE = 50

func _process(delta):
	var player = get_tree().get_nodes_in_group("player")[0]
	var camera = player.get_node("camera")
	
	var markers = get_children()
	
	var burnables = get_tree().get_nodes_in_group("burnable")
	for burnable in burnables:
		if burnable.burning:
			if burnable.has_node("visibility_notifier") and burnable.get_node("visibility_notifier").is_on_screen():
				continue

			var relative_position = burnable.global_position - camera.get_camera_screen_center()
			var marker = markers.pop_front()
			if marker == null:
				marker = MARKER_SCENE.instance()
				add_child(marker)
			
			marker.global_rotation = relative_position.angle() - PI * 1.5
			
			if (abs(relative_position.x) / abs(relative_position.y)) > (930 / 490):
				marker.position.x = SAFEZONE
				if relative_position.x > 0:
					marker.position.x = WIDTH - SAFEZONE
				marker.position.y = (HEIGHT / 2) + ((HEIGHT / 2) - SAFEZONE) * (relative_position.y / abs(relative_position.x))
			else:
				marker.position.x = (WIDTH / 2) + ((WIDTH / 2) - SAFEZONE) * (relative_position.x / abs(relative_position.y))
				marker.position.y = SAFEZONE
				if relative_position.y > 0:
					marker.position.y = HEIGHT - SAFEZONE
			
			if burnable.is_in_group("tree"):
				marker.get_node("icon/icon_tree").visible = true
				marker.get_node("icon/icon_satellite").visible = false
			else:
				marker.get_node("icon/icon_tree").visible = false
				marker.get_node("icon/icon_satellite").visible = true
	
	for marker in markers:
		marker.queue_free()