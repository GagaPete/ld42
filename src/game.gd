extends Node2D

signal game_over

func _on_game_game_over():
	get_tree().change_scene("res://win_screen.tscn")