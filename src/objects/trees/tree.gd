extends StaticBody2D


export(Texture) var burned_texture
var burning = false setget set_burning
var burned = false

onready var _initial_modulate = $sprite.modulate

func set_burning(value):
	if burning == value:
		return

	burning = value
	$fire.emitting = burning
	if burning:
		$spread_timer.start()
		$sprite.modulate = $sprite.modulate.darkened(0.5)
	else:
		$spread_timer.stop()
		$sprite.modulate = _initial_modulate

func _on_spread_timer_timeout():
	for body in $spread_radius.get_overlapping_bodies():
		if body.is_in_group("tree"):
			if not body.burning:
				body.burning = true
	
	# TODO: Replace by burned tree (and remove all
	$sprite.texture = burned_texture
	remove_from_group("burnable")
	add_to_group("dead")
	$spread_radius.queue_free()
	$spread_timer.queue_free()
	$fire.emitting = false
	burned = true