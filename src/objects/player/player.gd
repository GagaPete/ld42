extends KinematicBody2D

const SPEED = 320

onready var pickables_in_range = []

func _physics_process(delta):
	$body/arm/watersplash.emitting = Input.is_action_pressed("game_extinguish")
	
	var motion = Vector2()
	
	if Input.is_action_pressed("game_left"):
		motion.x -= 1.0
		$body.scale.x = -1
	if Input.is_action_pressed("game_right"):
		motion.x += 1.0
		$body.scale.x = 1
	
	if Input.is_action_pressed("game_up"):
		motion.y -= 1.0
	if Input.is_action_pressed("game_down"):
		motion.y += 1.0
	
	if motion.length_squared() > 0:
		if not $torso_animation.is_playing():
			$torso_animation.play("walk")
	else:
		$torso_animation.stop()
	
	move_and_slide(motion.normalized() * SPEED)

func _on_pickup_area_body_entered(body):
	if body.is_in_group("pickable"):
		pickables_in_range.append(body)

func _on_pickup_area_body_exited(body):
	pickables_in_range.erase(body)
