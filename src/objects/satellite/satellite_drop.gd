extends Node2D

onready var players_in_range = []

const CRATER_SCENE = preload("res://objects/crater/crater.tscn")
const SATELLITE_SCENE = preload("res://objects/satellite/satellite.tscn")

func replace_with_satellite():
	var crater = CRATER_SCENE.instance()
	get_parent().add_child(crater)
	crater.global_position = global_position - Vector2(0, 30.5)
	
	var satellite = SATELLITE_SCENE.instance()
	get_parent().add_child(satellite)
	satellite.global_position = global_position
	
	var particles = $particles
	remove_child(particles)
	get_parent().add_child(particles)
	particles.global_position = global_position + Vector2(0, 0.5)
	
	for body in $impact_area.get_overlapping_bodies():
		if body.is_in_group("player"):
			get_node("/root/game").emit_signal("game_over")
	
	queue_free()

func _on_impact_area_body_entered(body):
	if not body.is_in_group("player"):
		# In case the destination is already used (by something except the player), remove
		queue_free()
