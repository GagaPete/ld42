extends StaticBody2D

const EXPLOSION_SCENE = preload("res://effects/explosion/explosion.tscn")

var burning = false setget set_burning

onready var _initial_modulate = $sprite.modulate

func _ready():
	set_burning(true)

func set_burning(value):
	if burning == value:
		return

	burning = value
	$fire.emitting = burning
	if burning:
		$explosion_timer.start()
		$sprite.modulate = $sprite.modulate.darkened(0.5)
	else:
		$explosion_timer.stop()
		$sprite.modulate = _initial_modulate

func _on_explosion_timer_timeout():
	var explosion = EXPLOSION_SCENE.instance()
	get_parent().add_child(explosion)
	explosion.global_position = global_position
	queue_free()
