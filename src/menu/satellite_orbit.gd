extends Node2D

func _ready():
	randomize()
	$anchor/sprite.position.y = (randi() % 400) - 2400
	$anchor/sprite.rotation_degrees = (randi() % 360)

func _on_animation_finished(anim_name):
	queue_free()