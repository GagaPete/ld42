extends Timer

const ORBIT_SCENE = preload("res://menu/satellite_orbit.tscn")
const GAME_SCENE = preload("res://game.tscn")

func _ready():
	for i in range(0, 15):
		var orbit = ORBIT_SCENE.instance()
		add_child(orbit)
		orbit.get_node("animation").seek(i)

func _on_orbit_timer_timeout():
	var orbit = ORBIT_SCENE.instance()
	get_parent().add_child(orbit)
	wait_time = 0.5 + ((randi() % 20) / 10)