extends CenterContainer

func _ready():
	$VBoxContainer/Label2.text = "During your fight against the flames, %d people were sent on a\nrescue. Those now have a chance to safe humanity." % autoload.recent_score

func _on_menu_button_pressed():
	get_tree().change_scene("res://menu/menu.tscn")
